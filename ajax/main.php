<?php
    session_start();
    error_reporting(0);  
    header('Content-Type: text/html; charset=utf-8', TRUE);
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
    header("Last-Modified: Mon, 26 Jul 1999 05:00:00 GMT"); // always modified
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header("Pragma: no-cache"); // HTTP/1.0
    
    require_once('../_init.php');
	//sleep(1);
		    
    if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
    {           
        
		$do = htmlspecialchars(trim($_POST['action']));		
		switch ($do) { 			
			case 'check_field':
				$y = intval($_POST['h']);
				$x = intval($_POST['w']);
				// 1 - крестик
				// 2 - нолик
				$user_id =  Game::CheckField($y, $x);
				if($user_id == 1 || $user_id == 2) {
					$_SESSION['game']['time_finish'] = time() - $_SESSION['game']['time_start'];
				}
				echo $user_id;
			break;

			case 'delete':
				Game::DelField();
			break;

			case 'add':
				$u_name = trim($_POST['u_name']);
				$time = $_SESSION['game']['time_finish'];
				$data = array();
				$data['user'] = $u_name;
				$data['time'] = $time;
				Game::AddResult($data);
			break;
			
		}		
    }
?>