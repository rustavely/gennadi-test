<?php
header("Last-Modified: " . gmdate("D, d M Y H:i:s"). " GMT");
session_start();
ob_start();
include_once("_init.php");

?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
        "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" version="XHTML+RDFa 1.0" dir="ltr">
    <head profile="http://www.w3.org/1999/xhtml/vocab">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>

    <body>

    <script src="js/jquery-2.0.3.min.js"></script>
    <script src="js/jquery-migrate-1.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/style.css" />

    <a href="/index.php" type="button" class="btn btn-success" style="margin: 10px 20px;">Играть</a>

    <h1>Таблица результатов</h1>
    <?
        $result = Game::ListResult();
        if(!empty($result)) {
            echo Game::CreateTableResult($result);
        } else {
            echo 'Результатов нет.';
        }
    ?>
    </body>
    </html>
<?
    if(MYSQL_BAG_STATUS) {
        echo bl_debug(true);
    }
?>