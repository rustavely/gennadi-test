<?php    
    error_reporting(E_ALL);
	define ('SITE_DIR',dirname(__FILE__));  
	define ('SITE_PATH', dirname(__FILE__));  
	define ('CACHE_MYSQL', false);
	define ('DEBAG_STATUS', false);

    include_once( SITE_DIR.'/phpBugLost.0.5.php');
    define ('MYSQL_BAG_STATUS', false);

    if ( get_magic_quotes_gpc() ) {
        function stripslashes_deep($value) {
            if( is_array($value) )
            {
                $value = array_map('stripslashes_deep', $value);
            }
            elseif ( !empty($value) && is_string($value) )
            {
                $value = stripslashes($value);
            }
            return $value;
        }
        $_POST    = stripslashes_deep($_POST);        
    }

    include_once( SITE_DIR.'/lib/lib.php');
    include_once( SITE_DIR.'/conf/mysql_settings.php');
    include_once( SITE_DIR.'/lib/class.MySQL.php');
    include_once( SITE_DIR.'/lib/class.Game.php');
?>