<? 
class Game{
	static private $_instance = null;
	static $fieldWidth = 20; // ширина игрового поля
	static $fieldHeight = 20; // высота игрового поля
	static $gameOver = 5; // число крестиков или ноликов в ряд для победы
	static $numLine = 3; // число крестиков, после которых ставим нолик

	public function __construct(){          
		if (self::$_instance == null){  

		}
	}  
	
	public static function init(){
		$class = __CLASS__;
		if(self::$_instance == null) self::$_instance = new $class();
	}
	
	public function CreateTable(){
		self::init();    
		$table = '<table class="table table-bordered">';
		for($i=1; $i<=self::$fieldHeight; $i++) {
			$table .= '<tr >';
				for($i2=1; $i2<=self::$fieldWidth; $i2++) {
					$table .= '<td id="field_'.$i.'_'.$i2.'" onclick="GetField(\''.$i.'\', \''.$i2.'\');"></td>';
				}
			$table .= '</tr>';
		}
		$table .= '</table>';

		return $table;
	}

	public function CheckField ($y, $x) {
		self::init();
		// 1 - крестик
		// 2 - нолик
		if(empty($_SESSION['game'][$y][$x])) {
			$_SESSION['game'][$y][$x] = 1;
			return self::checkLine($y,$x);
		}
	}

	private function checkLine($y,$x) {
		self::init();
		$start_x= 1;
		$start_y= 1;
		$result_1 = 0;
		$result_2 = 0;
		$type_x = 0;
		$stop = 0;

		// по горизонтали
		$start_x = $x - self::$gameOver;
		$start_x = ($start_x < 1) ? 1 : $start_x;

		for($i=$start_x; $i<$start_x+(self::$gameOver*2); $i++) {
			if(!empty($_SESSION['game'][$y][$i])){
				$result_1 = ($_SESSION['game'][$y][$i] == 1) ? $result_1+1 : 0;
				$result_2 = ($_SESSION['game'][$y][$i] == 2) ? $result_2+1 : 0;

				if($result_1 == 3) {
					$type_x = 1;
				}

				if($result_1 == self::$gameOver) {
					$stop = 1;
					return 1;
					break;
				} elseif($result_2 == self::$gameOver) {
					$stop = 1;
					return 2;
					break;
				}
			} else {
				$result_1 = 0;
				$result_2 = 0;
			}
		}

		// по вертикали
		$start_y = $y - self::$gameOver;
		$start_y = ($start_y < 1) ? 1 : $start_y;

		for($i=$start_y; $i<$start_y+(self::$gameOver*2); $i++) {
			if(!empty($_SESSION['game'][$i][$x])){
				$result_1 = ($_SESSION['game'][$i][$x] == 1) ? $result_1+1 : 0;
				$result_2 = ($_SESSION['game'][$i][$x] == 2) ? $result_2+1 : 0;

				if($result_1 == 3) {
					$type_x = 2;
				}

				if($result_1 == self::$gameOver) {
					$stop = 1;
					return 1;
					break;
				} elseif($result_2 == self::$gameOver) {
					$stop = 1;
					return 2;
					break;
				}
			} else {
				$result_1 = 0;
				$result_2 = 0;
			}
		}

		// по диагонали ->
		$start_x = $x - self::$gameOver + 1;
		$start_x = ($start_x < 1) ? 1 : $start_x;
		$start_y = $y - self::$gameOver + 1;
		$start_y = ($start_y < 1) ? 1 : $start_y;

		$i2=$start_y;
		for($i=$start_x; $i<$start_x+(self::$gameOver*2); $i++) {
			if(!empty($_SESSION['game'][$i2][$i])){
				$result_1 = ($_SESSION['game'][$i2][$i] == 1) ? $result_1+1 : 0;
				$result_2 = ($_SESSION['game'][$i2][$i] == 2) ? $result_2+1 : 0;

				if($result_1 == 3) {
					$type_x = 3;
				}

				if($result_1 == self::$gameOver) {
					$stop = 1;
					return 1;
					break;
				} elseif($result_2 == self::$gameOver) {
					$stop = 1;
					return 2;
					break;
				}
			} else {
				$result_1 = 0;
				$result_2 = 0;
			}
			$i2 ++;
		}

		// по диагонали <-
		$start_x = $x - self::$gameOver + 1;
		$shift_y = ($start_x < 1) ? $start_x-1 : 0;
		$start_x = ($start_x < 1) ? 1 : $start_x;
		$start_y = $y + self::$gameOver - 1 + $shift_y;
		$start_y = ($start_y < 1) ? self::$fieldHeight : $start_y;


		$i2=$start_y;
		for($i=$start_x; $i<$start_x+(self::$gameOver*2); $i++) {
			if(!empty($_SESSION['game'][$i2][$i])){
				$result_1 = ($_SESSION['game'][$i2][$i] == 1) ? $result_1+1 : 0;
				$result_2 = ($_SESSION['game'][$i2][$i] == 2) ? $result_2+1 : 0;

				if($result_1 == 3) {
					$type_x = 4;
				}

				if($result_1 == self::$gameOver) {
					$stop = 1;
					return 1;
					break;
				} elseif($result_2 == self::$gameOver) {
					$stop = 1;
					return 2;
					break;
				}
			} else {
				$result_1 = 0;
				$result_2 = 0;
			}
			$i2 --;
			if($i2 < 0 ) {break;}
		}
		self::AddNolic ($y,$x, $type_x);

	}

	public function AddNolic($y,$x, $type_x) {
		self::init();

		if(empty($type_x)) {
			self::RandField();
		} elseif($type_x == 1) {
			$shift_x = $x+1;
			if($shift_x > self::$fieldWidth) {
				for($i=self::$fieldWidth; $i>0; $i--) {
					if(empty($_SESSION['game'][$y][$i])) {
						$shift_x = $i;
						break;
					}
				}
			} elseif(!empty($_SESSION['game'][$y][$shift_x])) {
				for($i=$shift_x; $i>0; $i--) {
					if(empty($_SESSION['game'][$y][$i])) {
						$shift_x = $i;
						break;
					}
				}
			}
			if(empty($_SESSION['game'][$y][$shift_x])) {
				$_SESSION['game'][$y][$shift_x] = 2;
				echo $y.'_'.$shift_x;
			} else {
				self::RandField();
			}
		} elseif($type_x == 2) {
			$shift_y = $y+1;
			if($shift_y > self::$fieldHeight) {
				for($i=self::$fieldHeight; $i>0; $i--) {
					if(empty($_SESSION['game'][$i][$x])) {
						$shift_y = $i;
						break;
					}
				}
			} elseif(!empty($_SESSION['game'][$shift_y][$x])) {
				for($i=$shift_y; $i>0; $i--) {
					if(empty($_SESSION['game'][$i][$x])) {
						$shift_y = $i;
						break;
					}
				}
			}
			if(empty($_SESSION['game'][$shift_y][$x])) {
				$_SESSION['game'][$shift_y][$x] = 2;
				echo $shift_y.'_'.$x;
			} else {
				self::RandField();
			}
		} elseif($type_x == 3) {
			$shift_x = $x+1;
			$shift_y = $y+1;

			if($shift_y > self::$fieldHeight || $shift_x > self::$fieldWidth) {
				for($i=1; $i<=self::$fieldWidth; $i++) {
					//echo $i."\n";
					if(empty($_SESSION['game'][$shift_y-$i][$shift_x-$i])) {
						$shift_x = $shift_x-$i;
						$shift_y = $shift_y-$i;
						break;
					}
				}
			}elseif(!empty($_SESSION['game'][$shift_y][$shift_x])) {
				for($i=1; $i<=self::$fieldWidth; $i++) {
					//echo $i."\n";
					if(empty($_SESSION['game'][$shift_y-$i][$shift_x-$i])) {
						$shift_x = $shift_x-$i;
						$shift_y = $shift_y-$i;
						break;
					}
				}
			}

			if(empty($_SESSION['game'][$shift_y][$shift_x])) {
				$_SESSION['game'][$shift_y][$shift_x] = 2;
				echo $shift_y.'_'.$shift_x;
			} else {
				self::RandField();
			}

		} elseif($type_x == 4) {
			$shift_x = $x+1;
			$shift_y = $y-1;

			if($shift_y < 0 || $shift_x > self::$fieldWidth) {

				for($i=1; $i<=self::$fieldWidth; $i++) {
					//echo $i."\n";
					if(empty($_SESSION['game'][$shift_y+$i][$shift_x-$i])) {
						$shift_x = $shift_x-$i;
						$shift_y = $shift_y+$i;
						break;
					}
				}
			}elseif(!empty($_SESSION['game'][$shift_y][$shift_x])) {
				for($i=1; $i<=self::$fieldWidth; $i++) {
					//echo $i."\n";
					if(empty($_SESSION['game'][$shift_y+$i][$shift_x-$i])) {
						$shift_x = $shift_x-$i;
						$shift_y = $shift_y+$i;
						break;
					}
				}
			}

			if(empty($_SESSION['game'][$shift_y][$shift_x])) {
				$_SESSION['game'][$shift_y][$shift_x] = 2;
				echo $shift_y.'_'.$shift_x;
			} else {
				self::RandField();
			}



		}
	}

	public function RandField () {
		for($i=1; $i<10; $i++) {
			$rand_x = rand(1, self::$fieldWidth);
			$rand_y = rand(1, self::$fieldHeight);
			if(empty($_SESSION['game'][$rand_y][$rand_x])) {
				$_SESSION['game'][$rand_y][$rand_x] = 2;
				echo $rand_y.'_'.$rand_x;
				break;
			}
		}
	}

	public static function ListResult(){
		self::init();

		$result = "
			SELECT *
				FROM ".DB_PREFIX."result
				ORDER BY time ASC
		";
		$row = MySQL::fetchAllArray($result);
		return $row;
	}

	public static function CreateTableResult($result){
		$table = '<table class="table table-bordered">';
		$table .=  '<tr><th style="text-align: center">Пользователь</th><th style="text-align: center">Затраченное время</th></tr>';
		foreach($result as $value) {
			$table .=  '<tr><td>'.$value['user'].'</td><td>'.$value['time'].' сек.</td></tr>';
		}

		$table .= '</table>';

		return $table;
	}

	public static function AddResult($data){
		self::init();
		MySQL::query("
			INSERT IGNORE INTO ".DB_PREFIX."result (user, time)
				VALUES (
					'".$data['user']."',
					'".$data['time']."'
				)
		");
		$user_id = MySQL::sql_insert_id();

		return $user_id;
	}

		public function DelField () {
		self::init();
		unset($_SESSION['game']);
			$_SESSION['game']['time_start'] = time();
	}
}
?>