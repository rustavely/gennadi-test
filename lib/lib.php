<?php 
	// Функция - определения формы слов
    // Например, "1 год, 2 года, 5 лет"
    // Пример вызова : decline('123', array('книга', 'книги', 'книг'));
	// $onlyword – если стоит true, то функция будет возвращать только существительное, идущее после числительного
    
    function decline($digit,$expr,$onlyword=false)
    {
        if(!is_array($expr)) $expr = array_filter(explode(' ', $expr));
        if(empty($expr[2])) $expr[2]=$expr[1];
        $i=preg_replace('/[^0-9]+/s','',$digit)%100;
        if($onlyword) $digit='';
        if($i>=5 && $i<=20) $res=$digit.' '.$expr[2];
        else
        {
            $i%=10;
            if($i==1) $res=$digit.' '.$expr[0];
            elseif($i>=2 && $i<=4) $res=$digit.' '.$expr[1];
            else $res=$digit.' '.$expr[2];
        }
        return trim($res);
	}
	
    //  Преобразует специальные символы в HTML сущности   
    function htmls($res, $stat = true) {          
        if($stat) {
            $res = trim($res);
        }
        return htmlspecialchars($res,ENT_QUOTES);
    }
	
	/* Метод преобразования даты 
    *  Зарезервированные переменные
    *  %d - день (число);
    *  %D - день недели;
    *  %B - день недели с большой буквы; 
    *  %m - номер месяца;
    *  %M - название месяца (родительный падеж);
    *  %Y - год; 
    *  %H - часы; 
    *  %i - минуты;
    *  %s - секунды;   
    */
    function format_date_rod_padej($date, $format = '%d %M %Y %H:%i, %B'){

        $months = array(
            'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'
        );

        $day_week_array = array(
            '0' => 'воскресенье',
            '1' => 'понедельник',
            '2' => 'вторник',
            '3' => 'среда',
            '4' => 'четверг',
            '5' => 'пятница',
            '6' => 'суббота',
        );
        
        $day_week_array_big = array(
            '0' => 'Воскресенье',
            '1' => 'Понедельник',
            '2' => 'Вторник',
            '3' => 'Среда',
            '4' => 'Четверг',
            '5' => 'Пятница',
            '6' => 'Суббота',
        );
        
        $date = strtotime($date);
                
        $day = date("d", $date);
        $month = date('m', $date);
        $month_name = $months[(int)date('m', $date)-1];
        $year = date('Y', $date);
        $day_week = $day_week_array[date('w', $date)];
        $day_week_big = $day_week_array_big[date('w', $date)];
        $full_time = strftime("%H:%M:%S",$date); 
        $h = date('H', $date); 
        $m = date('i', $date); 
        $s = date('s', $date); 
        
        $format_per= array('%d', '%D', '%B', '%m', '%M', '%Y', '%H', '%i', '%s');        
        $format_replace = array($day, $day_week, $day_week_big, $month, $month_name, $year, $h, $m, $s);
        $date_view = str_replace($format_per, $format_replace, $format);
                
        return $date_view;          
    } 
	    
    //Функция выводит var_dump в pre
    function v_dump() {
        $return = "";
        foreach (func_get_args() as $k => $v)
            $return.=var_export($v,true);
        echo "<div style='position:absolute; top:0px; right:0px;z-index:1000; width:auto; background: #333333; padding:5px 15px;'><pre style='background: #ccc; padding:10px;'><b style='color:#000;'>".$return."</b></pre></div>";
        return $return;
    }
    
    //Функция выводит var_dump в pre
    function html_dump() {
        $return = "";
        foreach (func_get_args() as $k => $v)
            $return.=var_export($v,true);
        echo "<pre style=\"background: #FFFFFF\">".$return."</pre>";
        return $return;
    }
    
    // Функция для работы с постраничкой  в Smarty
    function SitePager($list, $events_numbers, $current_page = 1, $page_numbers = 10) {
        global $smarty;
        $total_pages =  ceil($list/$events_numbers);
        
        $smarty->assign('currentPage',$current_page); // Текущая страница
        $smarty->assign('pageNumbers',$page_numbers); // Сколько страниц на странице
        $smarty->assign('totalPages',$total_pages);  // Всего страниц
    }  
    
    // Функция создает новый файл или перезаписывает его
    function CreateNewFiles($fname, $contents) {
        global $smarty; 
        @chmod($fname, 0777);
        $f = fopen($fname, "w");
        $result = fwrite($f, $contents); 
        fclose($f);
         
        if (!$result)
        {             
            return false;  
        }  else {
            return true;
        } 
    } 
    
    // Пример convert('cp1251', 'utf-8', $arr)
    function convert($from, $to, $var)
    {
        if (is_array($var))
        {
            $new = array();
            foreach ($var as $key => $val)
            {
                $new[convert($from, $to, $key)] = convert($from, $to, $val);
            }
            $var = $new;
        }
        else if (is_string($var))
        {
            $var = iconv($from, $to, $var);
        }
        return $var;
    }    
    
    // Преобразование JSON в массив
    function json2array($json){  
       if(get_magic_quotes_gpc()){
          $json = stripslashes($json);
       }        
       //$json = substr($json, 1, -1);         
       $json = str_replace(array(":", "{", "[", "}", "]"), array("=>", "array(", "array(", ")", ")"), $json); 
       @eval("\$json_array = array({$json});");
       return @$json_array[0];
    } 
    
    // Функция определения адреса посетителя
    function getRealIpAddr()
    {
      if (!empty($_SERVER['HTTP_CLIENT_IP']))
      {
        $ip=$_SERVER['HTTP_CLIENT_IP'];
      }
      elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
      {
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
      }
      else
      {
        $ip=$_SERVER['REMOTE_ADDR'];
      }
      return $ip;
    }
    
    function connectsitePOST($PostData, $url = ''){   
        if(empty($url) || empty($PostData)) {  
            exit;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);          
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);          
        return trim($result);
    }
	
	function rus2lat($string){ 	
        $translit = array(   
            'а' => 'a',   'б' => 'b',   'в' => 'v',  
            'г' => 'g',   'д' => 'd',   'е' => 'e',  
            'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',  
            'и' => 'i',   'й' => 'j',   'к' => 'k',  
            'л' => 'l',   'м' => 'm',   'н' => 'n',  
            'о' => 'o',   'п' => 'p',   'р' => 'r',  
            'с' => 's',   'т' => 't',   'у' => 'u',  
            'ф' => 'f',   'х' => 'x',   'ц' => 'c',  
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shh',  
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'\'',  
            'э' => 'e\'',   'ю' => 'yu',  'я' => 'ya', 
            'А' => 'A',   'Б' => 'B',   'В' => 'V',  
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',  
            'Ё' => 'YO',   'Ж' => 'Zh',  'З' => 'Z',  
            'И' => 'I',   'Й' => 'J',   'К' => 'K',  
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',  
            'О' => 'O',   'П' => 'P',   'Р' => 'R',  
            'С' => 'S',   'Т' => 'T',   'У' => 'U',  
            'Ф' => 'F',   'Х' => 'X',   'Ц' => 'C',  
            'Ч' => 'CH',  'Ш' => 'SH',  'Щ' => 'SHH',  
            'Ь' => '\'',  'Ы' => 'Y\'',   'Ъ' => '\'\'',  
            'Э' => 'E\'',   'Ю' => 'YU',  'Я' => 'YA',  
        );		
		$string = strtr($string, $translit);
		
        return $string;
    }

    function GetPageUrl($str) {
        $str = trim(strip_tags($str));
        $string = htmlspecialchars_decode(mb_strtolower($str, 'UTF-8'));
        $path_link = rus2lat($string);
        $text = preg_replace('~([^\w]+)~msi', '-', $path_link);
        $text = preg_replace('~([-]+){2,100}~msi', '-', $text);
        $text = preg_replace('~(-)$~msi', '', $text);
        $text = preg_replace('~^(-)~msi', '', $text);
        return $text;
    }
?>