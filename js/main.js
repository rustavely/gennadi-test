GetField = function(h,w) {
    t = $('#field_'+h+'_'+w);
    text = t.html();
    if(text.trim() == '') {
        t.html('X');
        AddNolik(h,w);
    }
}

AddNolik = function(h,w) {
    url = '/ajax/main.php';
    $.post(url,
        {
            'action':'check_field',
            'h':h,
            'w':w
        },
            function(data) {
                //alert(data);
            if(data != ''){
                if(data == 1) {
                    show_confirm('Записать результат?');
                } else if(data == 2) {
                    alert ('Выиграл компьютер');
                } else {
                    $('#field_'+data).html('0');
                }
            }
        }
    );
    /*
    w = parseFloat(w) +1;
    t = $('#field_'+h+'_'+w);
    t.html('O');
    */
}

    DelTable = function(){
        url = '/ajax/main.php';
        $.post(url,
            {
                'action':'delete'
            },
            function(data) {
                $('table td').html('');
            }
        );
    }

function show_confirm()
{
    var r=confirm("Добавить результат?");
    if (r==true)
    {
        $('#myModal').modal('show');

    }
    else
    {
        DelTable();
    }
}

function AddToBase() {
    u_name = $('#us_name').val();

    url = '/ajax/main.php';
    $.post(url,
        {
            'action':'add',
            'u_name':u_name
        },
        function(data) {
            DelTable();
            $('#myModal').modal('hide');
        }
    );
}