<?php
    header("Last-Modified: " . gmdate("D, d M Y H:i:s"). " GMT");   
    session_start(); 
    ob_start();
    include_once("_init.php");

?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
        "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" version="XHTML+RDFa 1.0" dir="ltr">
    <head profile="http://www.w3.org/1999/xhtml/vocab">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>

    <body>

    <script src="js/jquery-2.0.3.min.js"></script>
    <script src="js/jquery-migrate-1.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/style.css" />

    <?
        Game::DelField ();
        echo Game::CreateTable();
    ?>
    <button type="button" class="btn btn-success" style="margin: 10px 20px;" onclick="DelTable();">Играть</button>
    <a href="/result.php" type="button" class="btn btn-info" style="margin: 10px 20px;">Таблица результатов</a>




    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Добавить результат</h4>
                </div>
                <div class="modal-body" id="modal-body-reg">
                    <form class="form-horizontal" id="reg_form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Ваше Имя:</b></label>
                            <div class="col-sm-8">
                                <input type="text" class="text-input form-control" name="u_name" id="us_name" autocomplete="off" placeholder="Ваше Имя">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0);" class="btn btn-default" onclick="$('#myModal').modal('hide');">Закрыть</a>
                    <a href="javascript:void(0);" class="btn btn-primary" onclick="AddToBase();">Записаться</a>
                </div>
            </div>
        </div>
    </div>




    </body>
    </html>
<?
	if(MYSQL_BAG_STATUS) { 
		echo bl_debug(true); 		
	}	
?>